# The brand new Okular feature to sign PDFs

Digitally signing PDF documents instead of printing, manually signing and scanning the document again is pretty neat. It has been less convenient under Linux but not any longer ...

ZIH / TU Dresden sponsored the extension of the Okular PDF viewer so that it can add digital signatures and visible stamps. It was implemented by Klarälvdalens Datakonsult AB, KDAB.com and is now available in the master branches of libpoppler and okular.

See [https://tud.link/ndtb](https://tud.link/ndtb) ([Links to TUD Cloudstore](https://cloudstore.zih.tu-dresden.de/index.php/s/j5BKKyJYZFBzGsB)) for instructions.

The text files but not the large container file are also available in the [TU Chemnitz gitlab](https://gitlab.hrz.tu-chemnitz.de/s1809560--tu-dresden.de/tu-dresden-intern/-/tree/master/okular). Only with a login, not publicly.


## How to get it

There are several ways to get it before it will be in the official releases in January 2021 (poppler 21.01) and April 2021 (okular 21.04). 

Build yourself from [https://gitlab.freedesktop.org/poppler/poppler.git](https://gitlab.freedesktop.org/poppler/poppler.git) and [https://invent.kde.org/graphics/okular.git](https://invent.kde.org/graphics/okular.git). The modified version of Poppler is needed by Okular. Use the provided build scripts or instructions below. Please contribute more ways for your prefered Linux distribution to [andreas.knuepfer@tu-dresden.de](mailto:andreas.knuepfer@tu-dresden.de) to be added here.

### Build script for Ubuntu

For Ubuntu and similar use `okular_sign_install_ubuntu.sh` or redo the steps in it.

**Tested with**: Ubuntu 20.10, ...

### Build for Arch

Install 'poppler-git', 'poppler-qt5-git', and 'okular-git' through AUR **separately** (not in one step!).


### Build as Singularity container

Use the given singularity container as 

    %> ./okular_sign.sif

You can add a PDF file as argument. Was built and tested with singularity 2.6.1 on Ubuntu . See `okular_sign_2020-11.def` for the recipe.

Build your own Singularity container with either of:

    %> sudo singularity build okular_sign.sif okular_sign_singularity_container.def
    %> sudo singularity build --sandbox okular_sign.sandbox okular_sign_singularity_container.def

**Tested with**: Singularity 2.6.1 on Ubuntu 20.10 as host with Ubuntu 20.10 and 20.04 as guest.


### The KDE way with kdesrc-build

There is the recommended way how to compile your own KDE applications according to [https://community.kde.org/Get_Involved/development](https://community.kde.org/Get_Involved/development). It downloads and builds way more than you need. Use carefully and only of you have a couple of GB to spare.

**Tested with**: Ubuntu 20.10, ...


## Settings

### Configuration (needed the first time only)

Open Okular once to configure the certificate locations:

* Got to menu --> Settings --> Configure Backends / Einstellungen --> Anzeigemodule einrichten 
* Go to PDF Certificates 
* Set the path to your certificate database. Default is the Firefox settings directory ~/.mozilla/firefox/*.default. Keep this if your private key is to be found there.
* You may change it to the Thunderbird settings directory ~/.thunderbird/*.default. If you are using Thunderbird to sign emails with the very same private key, then ist will be found there.
* **Restart Okular**. Then you will be shown the available keys in the same configuration window. If your private key is shown, then you are good to sign PDFs


### Signing PDFs

* Open a PDF
* Click 'Digitally sign' in the 'Tools' menu. Alternatively add the "Digitally sign" icon in your prefered tool bar and click it.
* Draw a rectancle where you want to have the visible hint for the electronic signature.
* A dialog will ask for the private key to use, in case there are multiple. Select the one you want to use.
* Enter the passphrase of your key.
* Done


## Check

* Check the electronic signature with this version of Okular or your system's Okular or Libreoffice or [https://validator.docusign.com/](https://validator.docusign.com/) or Acrobat under Windows.
* The visible hint for the electronic signature is visible with any PDF viewer ... well, with most at least because Firefox doesn't always show it but Firefox is picky with other PDF files, too.


## Kudos and additions

I thank the author Albert Astals Cid for the great and professional work!

All issues for the software should be reported via the repositories mentioned above. Note that there are few minor issues open that will be fixed soon.

All issues with the provided instructions to build it for yourself or additions for other Linuxes and more ways to build it should be send to [andreas.knuepfer@tu-dresden.de](mailto:andreas.knuepfer@tu-dresden.de).

Best, Andreas

